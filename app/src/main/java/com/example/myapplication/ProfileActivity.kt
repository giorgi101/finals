package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

class ProfileActivity : AppCompatActivity() {
    private lateinit var buttonLogout: Button
    private lateinit var buttonChangePassword: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()

        registerListeners()
    }

    private fun init(){
        buttonLogout = findViewById(R.id.buttonLogout)
        buttonChangePassword = findViewById(R.id.buttonChangePassword)
    }

    private fun registerListeners(){
        buttonChangePassword.setOnClickListener {
            startActivity(Intent(this, PasswordChangeActivity::class.java))
        }
        buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}